import callWebApi from '../helpers/webApiHelper';

export const getAllMessages = async () => {
  const response = await callWebApi({
    endpoint: '/api/messages',
    type: 'GET'
  });
  return response.json();
};

export const getMessage = async id => {
  const response = await callWebApi({
    endpoint: `/api/messages/${id}`,
    type: 'GET'
  });
  return response.json();
};

export const addMessage = async request => {
  const response = await callWebApi({
    endpoint: '/api/messages',
    type: 'POST',
    request
  });
  return response.json();
};

export const updateMessage = async ({ messageId, ...updatedMessage }) => {
  const response = await callWebApi({
    endpoint: `/api/messages/${messageId}`,
    type: 'PUT',
    request: updatedMessage
  });
  return response.json();
};

export const deleteMessage = async messageId => {
  const response = await callWebApi({
    endpoint: `/api/messages/${messageId}`,
    type: 'DELETE'
  });
  return response.json();
};

export const likeMessage = async (messageId, user, isLike) => {
  const response = await callWebApi({
    endpoint: '/api/messages/react',
    type: 'PUT',
    request: {
      messageId,
      user,
      isLike
    }
  });
  return response.json();
};

// should be replaced by approppriate function
export const getMessageByHash = async hash => getMessage(hash);
