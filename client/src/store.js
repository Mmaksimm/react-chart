import { connectRouter, routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import { createStore, applyMiddleware, combineReducers } from "redux";
import createSagaMiddleware from 'redux-saga';
import { all } from 'redux-saga/effects';

import chartReducer from './containers/Chart/reducer';
import profileReducer from './containers/Profile/reducer';

import chartSagas from './containers/Chart/sagas';
import profileSagas from './containers/Profile/sagas'

const sagaMiddleware = createSagaMiddleware();
export const history = createBrowserHistory();

const rootReducer = combineReducers({
  chart: chartReducer,
  profile: profileReducer,
  router: connectRouter(history)
});

const rootSaga = function* () {
  yield all([
    ...chartSagas,
    ...profileSagas
  ])
};

const initialState = {
  chart: {
    confirmDelete: {
      isConfirmDelete: false
    },
    editMessage: {
      isEditMessage: false
    }
  }
};

const composedMiddleware = applyMiddleware(
  sagaMiddleware,
  routerMiddleware(history)
);

const store = createStore(
  rootReducer,
  initialState,
  composedMiddleware
);

sagaMiddleware.run(rootSaga);

export default store;
