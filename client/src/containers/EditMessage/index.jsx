import React, { useState } from 'react'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Button, Icon, Modal, Form } from 'semantic-ui-react'
import { updateMessage, cancel } from '../Chart/actions';

import styles from './styles.module.scss';

const EditMessage = ({
  isEditMessage,
  message,
  cancel: cancelEdit,
  updateMessage: refreshMessage
}) => {

  const [getEditText, setEditText] = useState(message.text);

  const HandlerOnChange = event => {
    setEditText(event.target.value);
  }

  const HandlerUpdateMessage = event => {
    event.preventDefault();

    refreshMessage({
      messageId: message.id,
      text: getEditText
    })
  }

  return (
    <Modal
      open={isEditMessage}
    >
      <Modal.Header className={styles.modalHeader}>Edit your message</Modal.Header>
      <Modal.Content className={styles.modalContent}>

        <Modal.Description>
          <Form >
            <Form.TextArea
              name="body"
              value={getEditText}
              placeholder="What is the news?"
              onChange={HandlerOnChange}
            />
          </Form>
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions className={styles.modalAction}>
        <Button.Group>
          <Button icon onClick={HandlerUpdateMessage}>
            <Icon name="save" />
          </Button>
          <Button icon onClick={cancelEdit}>
            <Icon name="window close" />
          </Button>
        </Button.Group>
      </Modal.Actions>
    </Modal>
  )
}

EditMessage.propTypes = {
  isEditMessage: PropTypes.bool.isRequired,
  message: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.objectOf(PropTypes.string)
    ])
  ).isRequired,
  cancel: PropTypes.func.isRequired,
  updateMessage: PropTypes.func.isRequired,
}

const mapStateToProps = (rootState) => ({
  isEditMessage: rootState.chart.editMessage.isEditMessage,
  message: rootState.chart.editMessage.dataEditMessage,
});

const actions = {
  cancel,
  updateMessage,
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditMessage);
