/* eslint-disable import/no-anonymous-default-export */
import { SET_USER, ERROR } from './actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_USER:
      return {
        ...state,
        user: action.user,
        isAuthorized: Boolean(action.user?.id),
        isLoading: false
      };

    case ERROR:
      return {
        ...state,
        error: action.payload.error
      };
    default:
      return state;
  }
};
