import { call, put, takeEvery } from 'redux-saga/effects';
import * as authService from '../../services/authService';
import {
  SET_USER,
  LOGIN,
  REGISTER,
  LOGOUT,
  LOAD_CURRENT_USER,
  ERROR
} from './actionTypes';

const setToken = token => localStorage.setItem('token', token);

const setUser = user => ({
  type: SET_USER,
  user
});

function* login({ payload: { request } }) {
  try {
    const { user, token } = yield call(authService.login, request);
    yield call(setToken, token);
    yield put(setUser(user));
  } catch (error) {
    yield put({ type: ERROR, payload: { error } })
  }
}

function* watchLogin() {
  yield takeEvery(LOGIN, login)
}

function* register({ payload: { request } }) {
  try {
    const { user, token } = yield call(authService.registration, request);
    yield call(setToken, token);
    yield put(setUser(user));
  } catch (error) {
    yield put({ type: ERROR, payload: { error } })
  }
}

function* watchRegister() {
  yield takeEvery(REGISTER, register)
}

function* logout() {
  yield call(setToken, '');
  yield put(setUser(null));
};

function* watchLogout() {
  yield takeEvery(LOGOUT, logout)
}

function* loadCurrentUser() {
  try {
    const user = yield call(authService.getCurrentUser);
    yield put(setUser(user));
  } catch (error) {
    yield put({ type: ERROR, payload: { error } })
  }
}

function* watchLoadCurrentUser() {
  yield takeEvery(LOAD_CURRENT_USER, loadCurrentUser)
}

const profileSagas = [
  watchLogin(),
  watchRegister(),
  watchLogout(),
  watchLoadCurrentUser(),
];

export default profileSagas
