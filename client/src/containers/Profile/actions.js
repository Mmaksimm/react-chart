import {
  LOGIN,
  REGISTER,
  LOGOUT,
  LOAD_CURRENT_USER
} from './actionTypes';

export const login = request => ({
  type: LOGIN,
  payload: { request }
});

export const register = request => ({
  type: REGISTER,
  payload: { request }
});

export const logout = () => ({
  type: LOGOUT,
});
export const loadCurrentUser = () => {
  return ({
    type: LOAD_CURRENT_USER
  })
};
