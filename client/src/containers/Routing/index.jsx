import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { Label, Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';
import Chart from '../../containers/Chart';
import LoginPage from '../../containers/LoginPage';
import Profile from '../../containers/Profile';
import Spinner from '../../components/Spinner';
import NotFound from '../../components/NotFound';
import PrivateRoute from '../../containers/PrivateRoute';
import PublicRoute from '../../containers/PublicRoute';
import EditMessage from '../EditMessage';
import { loadCurrentUser, logout } from '../../containers/Profile/actions';
import PropTypes from 'prop-types';


import styles from './styles.module.scss';

class Routing extends Component {

  componentDidMount() {
    if (!this.props.isAuthorized) {
      this.props.loadCurrentUser();
    }
  }

  render() {
    console.log(this.props.user);
    return (
      this.props.isLoading
        ? <Spinner />
        : (
          <div>
            {this.props.isAuthorized && (
              <>
                {this.props.user.isAdmin && (
                  <Label
                    basic
                    size="huge"
                    as="a"
                    onClick={this.props.logout}
                    className={styles.admin}
                  >
                    <Icon name="address card" />
                  </Label>
                )}
                <Label
                  basic
                  size="small"
                  as="a"
                  onClick={this.props.logout}
                  className={styles.signout}
                >
                  <Icon name="sign-out" />
                </Label>
              </>
            )}
            <main>
              <Switch>
                <PublicRoute exact path="/login" component={LoginPage} />
                <PrivateRoute exact path="/" component={Chart} />
                <PrivateRoute exact path="/edit" component={EditMessage} />
                <PrivateRoute exact path="/profile" component={Profile} />
                <Route path="*" exact component={NotFound} />
              </Switch>
            </main>
          </div>
        )
    )
  }
}


Routing.defaultProps = {
  modal: false,
  deleteFunction: () => { },
  deleteData: {},
  deleteMessage: ''
};

Routing.propTypes = {
  isAuthorized: PropTypes.bool,
  logout: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any),
  isLoading: PropTypes.bool,
  loadCurrentUser: PropTypes.func.isRequired
};

Routing.defaultProps = {
  isAuthorized: false,
  user: {},
  isLoading: true
};

const actions = {
  loadCurrentUser,
  logout,
};

const mapStateToProps = rootState => ({
  isAuthorized: rootState.profile.isAuthorized,
  user: rootState.profile.user,
  isLoading: rootState.profile.isLoading
});

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Routing);
