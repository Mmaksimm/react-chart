/* eslint-disable no-undef */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Container, Card } from 'semantic-ui-react';
import getStartDate from '../../helpers/getStartDate';
import Spinner from '../../components/Spinner';
import Message from '../../components/Message';
import Header from '../../components/Header';
import NewMessage from '../../components/NewMessage';
import Divider from '../../components/Divider';
import ConfirmDelete from '../../components/ConfirmDelete';
import EditMessage from '../EditMessage';
import Notifications from '../../components/Notifications';

import {
  setMessagesAction,
  addMessage,
  likeMessage,
  deleteMessage,
  modalConfirm,
  closeConfirm,
  editMessage,
  cancel,
  updateMessage,
  loadAllMessages,
  applyMessage,
  refreshMessage,
  removeMessage
} from './actions';

import styles from './styles.module.scss';

class Chart extends Component {
  constructor(props) {
    super(props);
    this.handlerUpdateMessage = this.handlerUpdateMessageMessage.bind(this)
  }

  componentDidMount() {
    this.props.loadAllMessages();
  }

  handlerUpdateMessageMessage(event, oldMessage, text) {
    event.preventDefault();
    const updatedMessage = {
      ...oldMessage,
      text,
      editedAt: new Date().toISOString()
    }
    this.setState({ messages: this.state.messages.map(message => message.id !== oldMessage.id ? message : updatedMessage) })
  }

  render() {
    let date = (new Date()).toISOString();
    const messages = this.props.messages;
    const user = this.props.ownUser;

    return (
      <>
        <Container>
          <Header messages={messages} />
          <Card className={styles.card}>
            {
              messages.length
                ? (
                  messages
                    .sort((messagePrev, message) => (Date.parse(message.createdAt) - Date.parse(messagePrev.createdAt)))
                    .map((message, index) => {
                      if (!index || (getStartDate(date) - getStartDate(message.createdAt)) >= 86400000) {
                        date = message.createdAt;
                        return (
                          <>
                            <Divider key={message.createdAt} date={message.createdAt} />
                            <Message
                              key={message.id}
                              message={message}
                              isMyMessage={this.props.userId === message.userId}
                              likeMessage={this.props.likeMessage}
                              modalConfirm={this.props.modalConfirm}
                              deleteMessage={this.props.deleteMessage}
                              editMessage={this.props.editMessage}
                            />
                          </>
                        )
                      }
                      return (
                        <Message
                          key={message.id}
                          message={message}
                          isMyMessage={this.props.ownUserId === message.userId}
                          likeMessage={this.props.likeMessage}
                          modalConfirm={this.props.modalConfirm}
                          deleteMessage={this.props.deleteMessage}
                          editMessage={this.props.editMessage}
                        />
                      )
                    })
                )
                : <Spinner />
            }
          </Card>
          <NewMessage addMessage={this.props.addMessage} />
        </Container>
        {this.props.isConfirmDelete && (
          <ConfirmDelete
            deleteFunction={this.props.deleteMessage}
            deleteData={this.props.deleteData}
            closeConfirm={this.props.closeConfirm}
          />
        )}
        {this.props.isEditMessage && (
          <EditMessage
            isEditMessage={this.props.isEditMessage}
            message={this.props.dataEditMessage}
            cancel={this.props.cancel}
            updateMessage={this.props.updateMessage}
          />
        )}
        <Notifications
          applyMessage={this.props.applyMessage}
          user={user}
          refreshMessage={this.props.refreshMessage}
          removeMessage={this.props.removeMessage}
        />
      </>
    )
  }
};

Chart.propTypes = {
  messages: PropTypes.arrayOf(
    PropTypes.objectOf(
      PropTypes.oneOfType(
        [
          PropTypes.string,
          PropTypes.number,
          PropTypes.objectOf(PropTypes.string)
        ]
      )
    )
  ),
  ownUserId: PropTypes.string,
  ownUser: PropTypes.objectOf(PropTypes.string),
  isConfirmDelete: PropTypes.bool,
  deleteData: PropTypes.objectOf(PropTypes.string),
  isEditMessage: PropTypes.bool,
  dataEditMessage: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.objectOf(PropTypes.string)
    ])
  ),
  setMessagesAction: PropTypes.func.isRequired,
  addMessage: PropTypes.func.isRequired,
  likeMessage: PropTypes.func.isRequired,
  deleteMessage: PropTypes.func.isRequired,
  modalConfirm: PropTypes.func.isRequired,
  closeConfirm: PropTypes.func.isRequired,
  editMessage: PropTypes.func.isRequired,
  cancel: PropTypes.func.isRequired,
  updateMessage: PropTypes.func.isRequired,
  loadAllMessages: PropTypes.func.isRequired,
  applyMessage: PropTypes.func.isRequired,
  refreshMessage: PropTypes.func.isRequired,
  removeMessage: PropTypes.func.isRequired,
};

Chart.defaultProps = {
  messages: [],
  isConfirmDelete: false,
  isEditMessage: false,
  dataEditMessage: {}
};

const mapStateToProps = (rootState) => ({
  messages: rootState.chart.messages,
  isConfirmDelete: rootState.chart.confirmDelete.isConfirmDelete,
  deleteData: rootState.chart.confirmDelete.deleteData,
  isEditMessage: rootState.chart.editMessage.isEditMessage,
  dataEditMessage: rootState.chart.editMessage.dataEditMessage,
  userId: rootState.profile.user.id
});

const actions = {
  setMessagesAction,
  addMessage,
  likeMessage,
  deleteMessage,
  modalConfirm,
  closeConfirm,
  editMessage,
  cancel,
  updateMessage,
  loadAllMessages,
  applyMessage,
  refreshMessage,
  removeMessage
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Chart);
