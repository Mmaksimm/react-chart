import {
  SET_ALL_MESSAGES,
  SET_MODAL_CONFIRM,
  CLOSE_CONFIRM,
  SET_EDIT_MESSAGE_ACTION,
  CANCEL_ACTIONS,
  ADD_MESSAGE,
  UPDATE_MESSAGE,
  DELETE_MESSAGE,
  LOAD_ALL_MESSAGES,
  APPLY_MESSAGE,
  REFRESH_MESSAGE,
  LIKE_MESSAGE,
  REMOVE_MESSAGE
} from './actionTypes';

export const setMessagesAction = messages => ({
  type: SET_ALL_MESSAGES,
  payload: messages
});

export const loadAllMessages = () => ({
  type: LOAD_ALL_MESSAGES
});

export const likeMessage = (messageId, user, isLike) => ({
  type: LIKE_MESSAGE,
  payload: { messageId, user, isLike }
});

export const modalConfirm = confirmDelete => {
  return {
    type: SET_MODAL_CONFIRM,
    payload: { confirmDelete }
  };
};

export const closeConfirm = () => ({
  type: CLOSE_CONFIRM
});

export const deleteMessage = ({ messageId }) => ({
  type: DELETE_MESSAGE,
  payload: { messageId }
});

export const removeMessage = messageId => ({
  type: REMOVE_MESSAGE,
  payload: { messageId }
});

export const editMessage = message => ({
  type: SET_EDIT_MESSAGE_ACTION,
  payload: { message }
});

export const cancel = () => ({
  type: CANCEL_ACTIONS
});

export const updateMessage = ({ messageId, text }) => ({
  type: UPDATE_MESSAGE,
  payload: { messageId, text }
});

export const refreshMessage = messageId => ({
  type: REFRESH_MESSAGE,
  payload: { messageId }
});

export const addMessage = message => ({
  type: ADD_MESSAGE,
  payload: { message }
});

export const applyMessage = messageId => ({
  type: APPLY_MESSAGE,
  payload: { messageId }
});