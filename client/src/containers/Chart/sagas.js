import { push } from 'connected-react-router';
import { call, put, takeEvery, select } from 'redux-saga/effects';
import { chartMessages } from './selectors';
import * as messageService from '../../services/messageService'
import {
  SET_ALL_MESSAGES,
  CLOSE_CONFIRM,
  CANCEL,
  ADD_MESSAGE,
  UPDATE_MESSAGE,
  DELETE_MESSAGE,
  LOAD_ALL_MESSAGES,
  APPLY_MESSAGE,
  REFRESH_MESSAGE,
  LIKE_MESSAGE,
  REMOVE_MESSAGE,
  SET_EDIT_MESSAGE,
  SET_EDIT_MESSAGE_ACTION,
  CANCEL_ACTIONS
} from './actionTypes';
// import { ADD_MESSAGE, UPDATE_MESSAGE, DELETE_MESSAGE, FETCH_MESSAGES } from "./actionTypes";

const setMessages = messages => ({ type: SET_ALL_MESSAGES, payload: { messages } });

export function* loadAllMessages() {
  try {
    const messages = yield call(messageService.getAllMessages);
    yield put(setMessages(messages));
  } catch (error) {
    console.log('fetchMessages error:', error.message)
  }
}

function* watchLoadAllMessages() {
  yield takeEvery(LOAD_ALL_MESSAGES, loadAllMessages)
}

export function* addMessage({ payload: { message } }) {
  try {
    const newMessage = yield call(messageService.addMessage, message);
    const messages = yield select(chartMessages);
    yield put(setMessages([newMessage, ...messages]));
  } catch (error) {
    console.log('createMessage error:', error.message);
  }
}

function* watchAddMessage() {
  yield takeEvery(ADD_MESSAGE, addMessage)
}

function* updateMessage({ payload: { messageId, text } }) {
  const messages = yield select(chartMessages);

  try {
    const { updatedAt } = yield call(messageService.updateMessage, { messageId, text });

    const mapMessages = message => ({
      ...message,
      text,
      updatedAt
    });

    const updated = messages.map(message => (message.id !== messageId ? message : mapMessages(message)));
    yield put(setMessages(updated));
    yield put({ type: CANCEL });
    yield put(push('/'));
  } catch (error) {
    console.log('updateMessage error:', error.message);
  }
}

function* watchUpdateMessage() {
  yield takeEvery(UPDATE_MESSAGE, updateMessage)
}

export function* deleteMessage({ payload: { messageId } }) {
  try {
    yield call(messageService.deleteMessage, messageId);
    const messages = yield select(chartMessages);
    const updated = messages.filter(message => (message.id !== messageId));
    yield put(setMessages(updated));
    yield put({ type: CLOSE_CONFIRM })
  } catch (error) {
    console.log('deleteMessage Error:', error.message);
  }
}

function* watchDeleteMessage() {
  yield takeEvery(DELETE_MESSAGE, deleteMessage)
}

function* applyMessage({ payload: { messageId } }) {
  const messages = yield select(chartMessages);
  try {
    const newMessage = yield call(messageService.getMessage, messageId);
    yield put(setMessages([newMessage, ...messages]));
  } catch (error) {
    console.log('applyMessage Error:', error.message);
  }
};

function* watchApplyMessage() {
  yield takeEvery(APPLY_MESSAGE, applyMessage)
}

function* refreshMessage({ payload: { messageId } }) {
  const messages = yield select(chartMessages);

  try {
    const updatedMessage = yield call(messageService.getMessage, messageId);
    const updated = messages.map(message => (message.id !== messageId ? message : updatedMessage));
    yield put(setMessages(updated));
  } catch (error) {
    console.log('refreshMessage Error:', error.message);
  }
};

function* watchRefreshMessage() {
  yield takeEvery(REFRESH_MESSAGE, refreshMessage)
}

function* likeMessage({ payload: { messageId, user, isLike } }) {
  const messages = yield select(chartMessages);

  try {
    const { diffLike, diffDislike } = yield call(messageService.likeMessage, messageId, user, isLike);

    const mapLikes = message => ({
      ...message,
      likeCount: Number(message.likeCount) + diffLike,
      dislikeCount: Number(message.dislikeCount) + diffDislike
    });

    const updated = messages.map(message => (message.id !== messageId ? message : mapLikes(message)));

    yield put(setMessages(updated));
  } catch (error) {
    console.log('likeMessage Error:', error.message);
  }
};

function* watchLikeMessage() {
  yield takeEvery(LIKE_MESSAGE, likeMessage)
}

function* removeMessage({ payload: { messageId } }) {
  const messages = yield select(chartMessages);
  const updated = messages.filter(message => (message.id !== messageId));
  yield put(setMessages(updated));
};

function* watchRemoveMessage() {
  yield takeEvery(REMOVE_MESSAGE, removeMessage)
}

function* editMessage({ payload: { message } }) {
  yield put({ type: SET_EDIT_MESSAGE, payload: { message } });
  yield put(push('/edit'));
};

function* watchEditMessage() {
  yield takeEvery(SET_EDIT_MESSAGE_ACTION, editMessage);
}

function* cancel() {
  yield put({ type: CANCEL });
  yield put(push('/'));
};

function* watchCancel() {
  yield takeEvery(CANCEL_ACTIONS, cancel);
}

const chartSagas = [
  watchLoadAllMessages(),
  watchAddMessage(),
  watchUpdateMessage(),
  watchDeleteMessage(),
  watchApplyMessage(),
  watchRefreshMessage(),
  watchLikeMessage(),
  watchRemoveMessage(),
  watchEditMessage(),
  watchCancel()
];

export default chartSagas;