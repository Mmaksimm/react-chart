/* eslint-disable import/no-anonymous-default-export */
import {
  SET_ALL_MESSAGES,
  SET_MODAL_CONFIRM,
  CLOSE_CONFIRM,
  SET_EDIT_MESSAGE,
  CANCEL
} from './actionTypes';


export default (state = {}, action) => {
  switch (action.type) {

    case SET_ALL_MESSAGES:
      console.log(action);
      return {
        ...state,
        messages: action.payload.messages,
      };

    case SET_MODAL_CONFIRM:
      return {
        ...state,
        confirmDelete: {
          ...action.payload.confirmDelete,
          isConfirmDelete: true
        }
      };

    case CLOSE_CONFIRM:
      return {
        ...state,
        confirmDelete: {
          isConfirmDelete: false,
          deleteData: {}
        }
      };

    case SET_EDIT_MESSAGE:
      return {
        ...state,
        editMessage: {
          isEditMessage: true,
          dataEditMessage: action.payload.message
        }
      };

    case CANCEL:
      return {
        ...state,
        editMessage: {
          isEditMessage: false,
          dataEditMessage: {}
        }
      };

    default:
      return state;
  }
};
