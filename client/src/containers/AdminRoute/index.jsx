import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

const PrivateRoute = ({ component: Component, isAuthorized, isAdmin, ...rest }) => (
  <Route
    {...rest}
    render={props => (isAdmin
      ? <Component {...props} />
      : isAuthorized
        ? <Redirect to={{ pathname: '/' }} />
        : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />)}
  />
);

PrivateRoute.propTypes = {
  isAuthorized: PropTypes.bool,
  isAdmin: PropTypes.bool,
  component: PropTypes.any.isRequired, // eslint-disable-line
  location: PropTypes.any // eslint-disable-line
};

PrivateRoute.defaultProps = {
  isAuthorized: false,
  isAdmin: false,
  location: undefined
};

const mapStateToProps = rootState => ({
  isAuthorized: rootState.profile.isAuthorized,
  isAdmin: rootState.profile.user.isAdmin
});

export default connect(mapStateToProps)(PrivateRoute);
