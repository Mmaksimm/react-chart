/* eslint-disable no-useless-constructor */
import React from 'react';
import PropTypes from 'prop-types';
import { Button, Label, Icon, Comment as MessageUI } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from '../../helpers/imageHelper';

import styles from './styles.module.scss';

const Message = ({
  message,
  modalConfirm,
  editMessage,
  isMyMessage,
  likeMessage,
}) => {

  const {
    id,
    text,
    createdAt,
    likeCount,
    dislikeCount,
    user
  } = message

  const handleDeleteMessage = event => {
    event.preventDefault();
    modalConfirm({
      deleteData: { messageId: message.id },
    });
  };


  const handlerEditMessage = () => {
    editMessage(message);
  }

  return (
    <div className={styles.separate}>
      <MessageUI.Group className={isMyMessage ? styles.myMessage : styles.message}>
        <MessageUI>
          <MessageUI.Avatar src={getUserImgLink(user.avatar)} className={styles.avatar} />
          <MessageUI.Content>
            <MessageUI.Author as="a">
              {user.login}
            </MessageUI.Author>
            <MessageUI.Metadata>
              {moment(createdAt).fromNow()}
            </MessageUI.Metadata>
            <MessageUI.Text>
              {text}
            </MessageUI.Text>
          </MessageUI.Content>
          <div className={styles.messageAction}>
            <Label
              basic
              size="small"
              as="a"
              className={isMyMessage ? styles.myMessageLike : styles.toolbarBtn}
              onClick={!isMyMessage ? () => likeMessage(id, user.id, true) : null}
            >
              <Icon name="thumbs up" />
              {likeCount}
            </Label>
            <Label
              basic
              size="small"
              as="a"
              className={isMyMessage ? styles.myMessageLike : styles.toolbarBtn}
              onClick={!isMyMessage ? () => likeMessage(id, user.id, false) : null}
            >
              <Icon name="thumbs down" />
              {dislikeCount}
            </Label>
            {
              isMyMessage && (
                <Button.Group floated="right" size="mini" className={styles.editMessage}>
                  <Button icon onClick={handlerEditMessage}>
                    <Icon name="cog" />
                  </Button>
                  <Button icon onClick={handleDeleteMessage}>
                    <Icon name="trash" />
                  </Button>)
                </Button.Group>
              )
            }
          </div>
        </MessageUI>
      </MessageUI.Group>
    </div>
  );
}

Message.propTypes = {
  message: PropTypes.objectOf(PropTypes.oneOfType(PropTypes.string)).isRequired,
  modalConfirm: PropTypes.func.isRequired,
  editMessage: PropTypes.func.isRequired,
  isMyMessage: PropTypes.bool.isRequired,
  likeMessage: PropTypes.func.isRequired
};

export default Message;
