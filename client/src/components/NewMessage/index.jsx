/* eslint-disable no-useless-constructor */
import React, { useState } from 'react'; import PropTypes from 'prop-types';
import { Input, Container, Form } from 'semantic-ui-react';

import styles from './styles.module.scss';

const NewMessage = ({ addMessage }) => {
  const [getNewMessage, setNewMessage] = useState('')

  const onChangeHandler = event => {
    setNewMessage(event.target.value);
  }

  const handlerAddMessage = async event => {
    event.preventDefault();
    if (!getNewMessage) {
      return;
    }
    await addMessage({ text: getNewMessage });
    setNewMessage('');
  };

  return (
    <Container className={styles.newMessage} >
      <Form onSubmit={handlerAddMessage}>
        <
          Input
          fluid
          action='Send'
          placeholder='New Message'
          value={getNewMessage}
          onChange={onChangeHandler}
        />
      </Form>
    </Container>
  )
};

NewMessage.propTypes = {
  addMessage: PropTypes.func.isRequired
}

export default NewMessage;