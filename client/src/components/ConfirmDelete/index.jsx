import React from 'react';
import PropTypes from 'prop-types';
import { Confirm } from 'semantic-ui-react';

const ConfirmDelete = ({
  deleteFunction,
  deleteData,
  closeConfirm
}) => {
  console.log(deleteData);
  const handlerDelete = async event => {
    event.preventDefault();
    await deleteFunction(deleteData);
    closeConfirm();
  };

  return (
    <Confirm
      open
      content='Do you want to delete the message?'
      onCancel={() => closeConfirm()}
      onConfirm={handlerDelete}
    />
  );
};

ConfirmDelete.propTypes = {
  deleteFunction: PropTypes.func.isRequired,
  deleteData: PropTypes.objectOf(PropTypes.string).isRequired,
  closeConfirm: PropTypes.func.isRequired
};

export default ConfirmDelete;
