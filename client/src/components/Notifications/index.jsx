import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import io from 'socket.io-client';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

const Notifications = ({
  user,
  applyMessage,
  refreshMessage,
  removeMessage
}) => {
  const [socket] = useState(io('http://localhost:3002'));

  useEffect(() => {
    if (!user) {
      return undefined;
    }
    const { id } = user;
    socket.emit('createRoom', id);

    socket.on('like', ({ messageId, message }) => {
      refreshMessage(messageId);
      NotificationManager.info(message);
    });

    socket.on('new_message', ({ userId, messageId, username }) => {
      if (userId !== id) {
        applyMessage(messageId);
        NotificationManager.info(`${username} created a message.`);
      }
    });

    socket.on('update_message', ({ userId, messageId, username = false }) => {
      if (userId !== id) {
        refreshMessage(messageId);
        if (username) NotificationManager.info(`${username} updated the message`);
      }
    });

    socket.on('delete_message', ({ userId, messageId, username }) => {
      if (userId !== id) {
        removeMessage(messageId);
        NotificationManager.info(`${username} deleted the message`);
      }
    });

    return () => {
      socket.close();
    };
  });

  return <NotificationContainer />;
};

Notifications.defaultProps = {
  user: undefined
};

Notifications.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  applyMessage: PropTypes.func.isRequired,
  refreshMessage: PropTypes.func.isRequired,
  removeMessage: PropTypes.func.isRequired
};

export default Notifications;
