import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Card, Container } from 'semantic-ui-react';

import styles from './styles.module.scss';

const Header = ({ messages }) => {
  const participants = [];
  messages.forEach(message => {
    if (participants.includes(message.userId)) return;
    participants.push(message.userId);
  })

  const numbersMessages = messages.length

  const dateLastMessage = messages.reduce((prevDate, message) => {
    if (Date.parse(message.createdAt) > Date.parse(prevDate)) return message.editedAt || message.createdAt
    return prevDate
  }, new Date(0).toISOString());

  return (
    <Container className={styles.header}>
      <Card.Content >
        <div className={styles.myPaddingLeft}>My Chart</div>
        <div className={styles.myPaddingLeft}>{participants.length} participants</div>
        <div className={styles.myPaddingLeft}>{numbersMessages} messages</div>
        <div className={styles.myPaddingRight}>Last message at {moment(dateLastMessage).fromNow()}</div>
      </Card.Content>
    </Container>
  )
};

Header.propTypes = {
  messages: PropTypes.arrayOf(
    PropTypes.objectOf(
      PropTypes.oneOfType(
        [
          PropTypes.string,
          PropTypes.number,
          PropTypes.objectOf(PropTypes.string)
        ]
      )
    )
  )
}

export default Header;