import fs from 'fs';
import express from 'express';
import cors from 'cors';
import path from 'path';
import passport from 'passport';
import http from 'http';
import socketIO from 'socket.io';
import helmet from 'helmet';
import morgan from 'morgan';
import routes from './api/routes/index';
import authorizationMiddleware from './api/middlewares/authorizationMiddleware';
import errorHandlerMiddleware from './api/middlewares/errorHandlerMiddleware';
import routesWhiteList from './config/routesWhiteListConfig';
import socketInjector from './socket/injector';
import socketHandlers from './socket/handlers';
import sequelize from './data/db/connection';
import './config/passportConfig';
import logger, { logStream } from './config/loggerConfig';

import { host, port, socketPort } from './config/config';

const app = express();
app.use(cors());
const socketServer = http.Server(app);
const io = socketIO(socketServer);

sequelize
  .authenticate()
  .then(() => {
    logger.info('Connected to the database.');
  })
  .catch(error => {
    logger.error('Could not connect to the database.', error);
  });

io.on('connection', socketHandlers);

app.use(helmet());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(morgan('combined', { stream: logStream }));

app.use(passport.initialize());

app.use(socketInjector(io));

app.use('/api/', authorizationMiddleware(routesWhiteList));

/*
app.use((req, res, next) => {
  req.user = {
    id: '2fd577bd-15bb-4c05-8fe8-f09287ee7de7',
    login: 'admin'
  };

  next();
});
*/

routes(app, io);

const staticPath = path.resolve(`${__dirname}/../client/build`);
app.use(express.static(staticPath));

app.get('*', (req, res) => {
  res.write(fs.readFileSync(`${__dirname}/../client/build/index.html`));
  res.end();
});

app.use(errorHandlerMiddleware);

app.listen(port, () => {
  logger.info(`Server started at ${host}:${port}`);
});

socketServer.listen(socketPort);
