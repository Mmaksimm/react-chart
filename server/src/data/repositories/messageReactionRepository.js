import { MessageReactionModel, MessageModel } from '../models/index';
import BaseRepository from './baseRepository';

class MessageReactionRepository extends BaseRepository {
  getMessageReaction(userId, messageId) {
    return this.model.findOne({
      group: [
        'messageReaction.id',
        'message.id'
      ],
      where: { userId, messageId },
      include: [{
        model: MessageModel,
        attributes: ['id', 'userId']
      }]
    });
  }
}

export default new MessageReactionRepository(MessageReactionModel);
