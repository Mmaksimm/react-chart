import { UserModel } from '../models/index';
import BaseRepository from './baseRepository';

class UserRepository extends BaseRepository {
  addUser(user) {
    return this.create(user);
  }

  getByLogin(login) {
    return this.model.findOne({ where: { login } });
  }

  getUserById(id) {
    return this.model.findOne({ where: { id } });
  }
}

export default new UserRepository(UserModel);
