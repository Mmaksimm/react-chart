const now = new Date();

const messagesSeed = [{
  login: 'Ruth',
  text: 'I don’t *** understand. It’s the Panama accounts'
},
{
  login: 'Wendy',
  text: 'Tells exactly what happened.'
},
{
  login: 'Wendy',
  text: 'You were doing your daily bank transfers and…'
},
{
  login: 'Ruth',
  text: 'Yes, like I’ve been doing every *** day without red *** flag'
},
{
  login: 'Ruth',
  text: 'There`s never been a *** problem.'
},
{
  login: 'Helen',
  text: 'Why this account?'
},
{
  login: 'Ruth',
  text: 'I don`t *** know! I don`t know!'
},
{
  login: 'Ben',
  text: 'What the ** is a red flag anyway?'
},
{
  login: 'Helen',
  text: 'You said you could handle things'
},
{
  login: 'Ruth',
  text: 'I did what he taught me.'
},
{
  login: 'Ruth',
  text: 'it`s not my fucking fault!'
},
{
  login: 'Wendy',
  text: 'Can you fix this? Can you fix it?'
},
{
  login: 'Helen',
  text: 'Her best is gonna get us all killed.'
},
{
  login: 'Ruth',
  text: 'I don`t know how!'
},
{
  login: 'Ruth',
  text: 'it means that the accounts frozen that cause the feds might think that there’s a crime being committed.'
},
{
  login: 'Ruth',
  text: 'Like by me'
},
{
  login: 'Ben',
  text: 'aaaha!'
}].map(message => ({
  ...message,
  createdAt: now,
  updatedAt: now
}));

export default messagesSeed;
