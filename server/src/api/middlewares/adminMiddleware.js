const adminMiddleware = (req, res, next) => {
  if (req.user.isAdmin) next();
  throw Error('No access rights');
};

export default adminMiddleware;
