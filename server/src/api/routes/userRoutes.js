import { Router } from 'express';
import * as userService from '../services/userService';

const router = Router();

router
  .get('/', (req, res, next) => userService.getUsers(req.query)
    .then(users => res.send(users))
    .catch(next))
  .get('/:id', (req, res, next) => userService.getUserById(req.params.id)
    .then(user => res.send(user))
    .catch(next))
  .post('/', (req, res, next) => userService.create(req.user.id, req.body)
    .then(({ newUser }) => res.send(newUser))
    .catch(next))
  .put('/:id', (req, res, next) => userService.updateUser(req.params.id, req.body)
    .then(({ updatedAt }) => res.send({ updatedAt }))
    .catch(next))
  .delete('/:id', (req, res, next) => userService.deleteUser(req.params.id)
    .then(() => res.send({ success: true }))
    .catch(next));

export default router;
