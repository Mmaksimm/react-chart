import userRepository from '../../data/repositories/userRepository';

export const getUserById = async userId => {
  const { id, login, avatar, isAdmin } = await userRepository.getUserById(userId);
  return { id, login, avatar, isAdmin };
};

export const getUsers = () => userRepository.getUsers();

export const create = async user => {
  const { id } = await userRepository.create(user);
  return {
    userId: id
  };
};

export const updateUser = async (userId, body) => {
  await userRepository.updateById(
    userId,
    body
  );
  const { updatedAt } = await getUserById(userId);
  return {
    updatedAt
  };
};

export const deleteUser = async userId => {
  await userRepository.deleteById(userId);
};
